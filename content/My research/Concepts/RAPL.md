---
title: Running Average Power Limit
aliases:
  - Running Average Power Limit
draft: false
tags:
  - seeds
---
The Running Average Power Limit (RAPL) is an interface for reporting the accumulated energy consumption of various power domains, for the most modern processors. The power domains can be CPU or RAM[^1].

Another definition will complete this first one[^2]: RAPL measures the electricity consumption of the CPU and more components thanks to sensors integrated into the system-on-chip, and exposes it to the operating system through model-specific registers (MSR) designed by the CPU manufacturer.


[^1]: From https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/running-average-power-limit-energy-reporting.html and https://powerapi.org/reference/formulas/rapl/
[^2]: Raffin, G., & Trystram, D. (2024). Dissecting the software-based measurement of CPU energy consumption: a comparative analysis. _arXiv preprint arXiv:2401.15985_.