---
title: Hardware consumption measurements
draft: false
tags:
  - seeds
  - hardware
---
When it comes to [[Measuring the energy consumption of a software|measuring software energy consumption]], there is always a moment when you realize you need to cover different hardware components consumption, because the software relies on hardware components and, obviously, you can't ignore that.

So, what are those hardware components ? It depends on your device but here are the main ones: CPU, RAM, storage, graphic cards and GPUs. The last one is more and more popular the last few years and becomes a huge interest in the area. We have now a non exhaustive list of hardware but how can we get the energy consumption of those ? This is the question and it's not as simple as that.

## Physical power meter
One solution is to use a physical power meter and plug it to the component we want to measure. It will tell us the exact power measures but it is really constraining as we have to open our device and connect the power meter to the component and it is not really scalable. Shit.

But do not despair! Some components provide interfaces that allow developers to get direct consumption data, provided by the hardware itself. It is less accurate than the power meter here but it is easier to use it. A huge example of this is the [[RAPL|Running Average Power Limit]] interface from Intel processors.