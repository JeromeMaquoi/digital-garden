---
title: Measuring the energy consumption of a software
draft: false
tags:
  - seeds
  - energy-consumption
---
*In the recent years, sustainability concerns have emerged in the software engineering community. However, few researches are focused on the link between the evolution of a source code and energy consumption. As a consequence, most developers don't have knowledge on the energy consumption of their software during its development. This article summarizes what I've found on the literature about the measurements of the energy consumption of a software. As my research progressed, different categories emerged, that will be discussed here: the goal of the measurement, the different methods to achieve it and a list of tools worth mentioning.*

***

As you may know, the climate and environmental crisis we face nowadays become more and more significant[^1]. The impact increase of IT in this crisis has been quite impressive the past decades, and it's not going down. One of the biggest issue of IT is its energy consumption[^2]. As computer scientists, as software engineers, we have a responsibility to try and solve this problem. And to do so, we have to understand what we are talking about. So, first of all, let's talk about energy and power.
## Energy ? Power ? What are those ?
Most of the papers and 
[This blog post](https://luiscruz.github.io/2023/05/13/energy-units.html), written by Luis Cruz, assistant Professor at TUDelft, specialist in Sustinable Software Engineering, explains very well the terms that are used in software engineering when you talk about energy. For those who don't want to read it (shame on you!), here are the most important definitions for us:
- **Energy** is the amount necessary work to make something. It is defined in *joules* (J) as the standard unit of energy. Another common unit is the kilowatt-hour (kWh) that represents the same parameter as *joule* but in much higher order of magnitude, and is calculated as follow: 1kWh = 3 600 000J
- **Power** is the amount of work done by a unit of time, in general a *second*. Using the standard SI units, 1 unit of power = 1 joule per second.

The utilization of energy or power depends on the situation. Let's take an example from Luis Cruz's blog:

> Using power units instead of energy units is useful in particular scenarios. Imagine that we want to measure the energy consumption of reading a book on a device. Saying that reading a book spends 10J does not say much about its energy efficiency – the user could be reading for 1 minute or 10 minutes. In those cases, it makes sense to talk about power consumption. On the other hand, if we talk about a Bitcoin transaction, we really don’t want to talk about power – we should rather be talking about the total energy consumption.

Okay but in software engineering, what's more important? Energy or consumption? Luis also addresses this question:
> We want to analyse the project by use case: for example, the energy consumption of applying a filter to a photo, uploading a file to the server, etc. Hence, as a rule of thumb, energy consumption is the parameter we should be reporting when testing energy in software projects.

Now that we have defined the terms, we can go further and search for the different methods to measure the consumption of a software.

## What is the goal of the measurements ?
What are we trying to achieve? Why are we making energy measurements? These are the question we have to ask before doing anything. 

## Methods to measure the consumption
There are different ways of tackling this problem.


[^1]: H.-O. Pörtner, D. C. Roberts, E. Poloczanska, et al., “Ipcc, 2022: Summary for policymakers,” 2022.
[^2]: R. Verdecchia, P. Lago, C. Ebert, and C. De Vries, “Green it and green software,” IEEE Software, vol. 38, no. 6, pp. 7–15, 2021.