---
title: Welcome to my garden
draft: false
tags:
  - seeds
---
This website contains some of the knowledge I acquired during my PhD thesis in computer science. I'm trying to make a knowledge website, for me and for those who are interested in what I'm working on.

I write the content of this knowledge website as a digital garden. If you are not familiar with this concept, I suggest you to read [this essay of Maggie Appleton](https://maggieappleton.com/garden-history).